module.exports = {
  mode: 'development',
  entry: { index: './src/index' },
  output: {
    path: __dirname + '/dist',
    filename: '[name].js'
  },
  resolve: { extensions: ['.ts', '.js', '.html'] },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        loader: 'awesome-typescript-loader'
      },
      {
        test: /\.html$/,
        exclude: /node_modules/,
        loader: 'html-loader?exportAsEs6Default'
      }
    ]
  },
  plugins: []
};