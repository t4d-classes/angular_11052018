import {
  CarActionTypes, CarActionUnion, AppendCarAction,
  DeleteCarAction, ReplaceCarAction,
} from '../car.actions';

import { Car } from '../models/car';

export const carsReducer = (state: Car[] = [], action: CarActionUnion) => {

  switch (action.type) {

    case CarActionTypes.APPEND:
      const carToAppend = {
        ...(action as AppendCarAction).payload,
        id: Math.max(...state.map(c => c.id), 0) + 1,
      };
      return [ ...state, carToAppend ];

    case CarActionTypes.DELETE:
      return state.filter(c => c.id !== (action as DeleteCarAction).payload);

    case CarActionTypes.REPLACE:
      const replaceCarAction = action as ReplaceCarAction;
      const carIndex = state.findIndex(c => c.id === replaceCarAction.payload.id);
      return [ ...state.slice(0, carIndex), replaceCarAction.payload, ...state.slice(carIndex + 1) ];

    default:
      return state;
  }


};
