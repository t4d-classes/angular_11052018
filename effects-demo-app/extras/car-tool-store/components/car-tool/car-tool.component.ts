import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';

import {
  AppendCarAction, EditCarAction, CancelCarAction,
  DeleteCarAction, ReplaceCarAction
} from '../../car.actions';

import { AppState } from '../../appState';

import { Car } from '../../models/car';

@Component({
  selector: 'car-tool',
  templateUrl: './car-tool.component.html',
  styleUrls: ['./car-tool.component.css']
})
export class CarToolComponent implements OnInit {

  public cars$: Observable<Car[]>;
  public editCarId$: Observable<number>;

  constructor(private store: Store<AppState>) {
    this.cars$ = this.store.pipe(select('cars'));
    this.editCarId$ = this.store.pipe(select('editCarId'));

    this.store.subscribe(state => console.log(state));
  }

  ngOnInit() {
  }

  doEditCar(carId: number) {
    this.store.dispatch(new EditCarAction(carId));
  }

  doDeleteCar(carId: number) {
    this.store.dispatch(new DeleteCarAction(carId));
  }

  doCancelCar() {
    this.store.dispatch(new CancelCarAction());
  }

  doSaveCar(car: Car) {
    this.store.dispatch(new ReplaceCarAction(car));
  }

  doAddCar(car: Car) {
    this.store.dispatch(new AppendCarAction(car));
  }

}
