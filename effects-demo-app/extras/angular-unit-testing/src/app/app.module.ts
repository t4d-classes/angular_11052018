import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { MyUppercasePipe } from './pipes/my-uppercase.pipe';
import { ToolHeaderComponent } from './components/tool-header/tool-header.component';


@NgModule({
  declarations: [
    AppComponent,
    MyUppercasePipe,
    ToolHeaderComponent,
  ],
  imports: [
    BrowserModule, HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
