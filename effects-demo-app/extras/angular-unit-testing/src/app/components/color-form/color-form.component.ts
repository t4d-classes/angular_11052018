import { Component, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'color-form',
    templateUrl: './color-form.component.html',
    styleUrls: ['./color-form.component.css'],
})
export class ColorFormComponent {

    public newColor = 'default color';

    @Output()
    public colorSubmitted = new EventEmitter<string>();

    public addNewColor() {
        this.colorSubmitted.emit(this.newColor);
    }
}
