import { Item } from '../../core/item';

export interface Color extends Item {
  name: string;
}
