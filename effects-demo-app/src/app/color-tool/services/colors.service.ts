import { Injectable } from '@angular/core';

import { DataService } from '../../core/data-service';

import { Color } from '../models/color';

@Injectable({
  providedIn: 'root',
})
export class ColorsService extends DataService<Color> {
  constructor() {
    super([
      { id: 1, name: 'red' },
      { id: 1, name: 'blue' },
      { id: 1, name: 'yellow' },
    ]);
  }
}
