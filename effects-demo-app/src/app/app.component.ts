import { Component } from '@angular/core';

import { of, Observable, Observer } from 'rxjs';
import { map, filter, tap, take, concat, merge } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'DemoApp';

  // constructor() {
  //   console.log('app comp constructor');

  //   const nums1$ = Observable.create( (observer: Observer<number>) => {

  //     let counter = 0;

  //     const h = window.setInterval(() => {
  //       counter++;
  //       observer.next(counter);
  //     }, 500);

  //     setTimeout(() => {
  //       window.clearInterval(h);
  //       observer.complete();
  //     }, 10000);

  //   });

  //   const nums2$ = Observable.create( (observer: Observer<number>) => {

  //     let counter = 2000;

  //     const h = window.setInterval(() => {
  //       counter++;
  //       observer.next(counter);
  //     }, 750);

  //     setTimeout(() => {
  //       window.clearInterval(h);
  //       observer.complete();
  //     }, 10000);

  //   });

  //   nums1$.pipe(
  //     merge(nums2$),
  //   ).subscribe(n => console.log(n));

  //   // const nums1$ = of(1, 2, 3, 4, 5);
  //   // const nums2$ = of('a', 'b', 'c', 'd', 'c');




  //   // const nums$ = of(1, 2, 3, 4, 5);

  //   // const doubleNums$ = nums$.pipe(
  //   //   tap(n => console.log('1st tap', n)),
  //   //   map(n => n * 2),
  //   //   // filter(n => n > 4),
  //   //   // tap(n => console.log('2nd tap', n)),
  //   //   take(3),
  //   // );

  //   // doubleNums$.subscribe(num => console.log('made it to the end', num));
  //   // // doubleNums$.subscribe(num => console.log('made it to the end', num));


  // }
}
