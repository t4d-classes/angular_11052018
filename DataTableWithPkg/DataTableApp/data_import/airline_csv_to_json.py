import pandas as pd


def snake_case_to_camel_case(s):
    parts = s.split('_')
    return parts[0] + "".join([ part.title() for part in parts[1:] ])

def main():

    airline_data_frame = pd.read_csv('./airline.csv', parse_dates=['FL_DATE'])

    airline_data_frame.drop(
        airline_data_frame.columns[len(airline_data_frame.columns)-1],
        axis=1,
        inplace=True
    )

    airline_data_frame.columns = [
        snake_case_to_camel_case(columnName.lower())
        for columnName in airline_data_frame.columns
    ]

    with open('../airline.json', 'w') as f:
        f.write('{"flights":' + airline_data_frame.to_json(orient='records') + '}')


if __name__ == '__main__':
    main()
