import { Component, forwardRef } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

import { FilterForm } from 'data-table';

@Component({
  selector: 'flight-filter-form',
  templateUrl: './flight-filter-form.component.html',
  styleUrls: ['./flight-filter-form.component.css'],
  providers: [
    { provide: FilterForm, useExisting: forwardRef(() => FlightFilterFormComponent) },
  ]
})
export class FlightFilterFormComponent extends FilterForm {

  public filterForm: FormGroup;

  constructor(private fb: FormBuilder) {
    super();

    this.filterForm = this.fb.group({
      filterValueInput: '',
    });

  }

  get filterValue() {
    return this.filterForm.value.filterValueInput;
  }

  public clearForm() {
    this.filterForm.reset();
  }

}
