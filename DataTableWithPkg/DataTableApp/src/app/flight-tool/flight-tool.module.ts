import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { DataTableModule } from 'data-table';

import { FlightHomeComponent } from './components/flight-home/flight-home.component';
import { FlightsService } from './services/flights.service';
import { FlightFilterFormComponent } from './components/flight-filter-form/flight-filter-form.component';

@NgModule({
  imports: [
    CommonModule, HttpClientModule, DataTableModule, ReactiveFormsModule,
  ],
  declarations: [FlightHomeComponent, FlightFilterFormComponent],
  exports: [FlightHomeComponent],
  providers: [FlightsService]
})
export class FlightToolModule { }
