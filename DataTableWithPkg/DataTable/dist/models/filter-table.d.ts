export declare abstract class FilterTable {
    private _filteredData;
    currentPage: number;
    protected _data: any[];
    filteredData: any[];
    readonly abstract allData: any[];
}
