export declare abstract class FilterForm {
    filterValue: any;
    abstract clearForm(): void;
}
