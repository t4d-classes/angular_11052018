var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Component, forwardRef, Input, ContentChild } from '@angular/core';
import { FilterTable } from '../../models/filter-table';
import { PageableTable } from '../../models/pageable-table';
var PaginatedTableComponent = (function (_super) {
    __extends(PaginatedTableComponent, _super);
    function PaginatedTableComponent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.pageLength = 10;
        _this.initialPage = 0;
        _this.listPropName = 'data';
        return _this;
    }
    Object.defineProperty(PaginatedTableComponent.prototype, "data", {
        get: function () {
            return this._data;
        },
        set: function (d) {
            this._data = d || [];
            this.pageableTable.dataPage = this.listPage;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PaginatedTableComponent.prototype, "allData", {
        get: function () {
            return this.pageableTable.allData;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PaginatedTableComponent.prototype, "listPage", {
        get: function () {
            var startIndex = this.currentPage * this.pageLength;
            var endIndex = startIndex + this.pageLength;
            return (this.filteredData || this.data).slice(startIndex, endIndex);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PaginatedTableComponent.prototype, "totalPages", {
        get: function () {
            return Math.ceil((this.filteredData || this.data).length / this.pageLength);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PaginatedTableComponent.prototype, "firstPage", {
        get: function () {
            return this.currentPage <= 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PaginatedTableComponent.prototype, "lastPage", {
        get: function () {
            return this.currentPage + 1 >= this.totalPages;
        },
        enumerable: true,
        configurable: true
    });
    PaginatedTableComponent.prototype.ngOnInit = function () {
        this.currentPage = this.initialPage;
    };
    PaginatedTableComponent.prototype.ngAfterContentInit = function () {
        this.data = this.pageableTable.allData;
    };
    PaginatedTableComponent.prototype.ngAfterContentChecked = function () {
        if (this.data !== this.pageableTable.allData) {
            this.data = this.pageableTable.allData;
            this.filteredData = null;
            this.currentPage = this.initialPage;
        }
        this.pageableTable.dataPage = this.listPage;
    };
    PaginatedTableComponent.prototype.go = function (pages) {
        this.currentPage += pages;
        this.pageableTable.dataPage = this.listPage;
    };
    PaginatedTableComponent.decorators = [
        { type: Component, args: [{
                    selector: 'paginated-table',
                    templateUrl: './paginated-table.component.html',
                    styleUrls: ['./paginated-table.component.css'],
                    providers: [
                        { provide: FilterTable, useExisting: forwardRef(function () { return PaginatedTableComponent; }) }
                    ],
                },] },
    ];
    /** @nocollapse */
    PaginatedTableComponent.ctorParameters = function () { return []; };
    PaginatedTableComponent.propDecorators = {
        "data": [{ type: Input },],
        "pageLength": [{ type: Input },],
        "initialPage": [{ type: Input },],
        "listPropName": [{ type: Input },],
        "pageableTable": [{ type: ContentChild, args: [PageableTable,] },],
    };
    return PaginatedTableComponent;
}(FilterTable));
export { PaginatedTableComponent };
//# sourceMappingURL=paginated-table.component.js.map