import { DataTableCol } from '../../models/data-table-col';
import { PageableTable } from '../../models/pageable-table';
export declare class DataTableComponent extends PageableTable {
    config: {
        cols: DataTableCol[];
    };
    data: any[];
}
