import { Injectable } from "@angular/core";
var Cars = (function () {
    function Cars() {
    }
    Cars.prototype.getAll = function () {
        return [
            { id: 1, make: 'Ford', model: 'Fusion', color: 'yellow', year: 2013, price: 12000 },
            { id: 2, make: 'Ford', model: 'F150', color: 'purple', year: 2014, price: 34000 },
        ];
    };
    Cars.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    Cars.ctorParameters = function () { return []; };
    return Cars;
}());
export { Cars };
//# sourceMappingURL=cars.service.js.map