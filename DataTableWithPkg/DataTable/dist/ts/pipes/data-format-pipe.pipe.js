import { Pipe } from '@angular/core';
var DataFormatPipePipe = (function () {
    function DataFormatPipePipe() {
    }
    DataFormatPipePipe.prototype.transform = function (value, args) {
        switch (args) {
            case 'date':
                return new Date(value).toDateString();
            default:
                return value;
        }
    };
    DataFormatPipePipe.decorators = [
        { type: Pipe, args: [{
                    name: 'dataFormatPipe'
                },] },
    ];
    /** @nocollapse */
    DataFormatPipePipe.ctorParameters = function () { return []; };
    return DataFormatPipePipe;
}());
export { DataFormatPipePipe };
//# sourceMappingURL=data-format-pipe.pipe.js.map