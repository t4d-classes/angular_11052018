import { Pipe } from "@angular/core";
var CapitalizePipe = (function () {
    function CapitalizePipe() {
    }
    CapitalizePipe.prototype.transform = function (value) {
        return value.slice(0, 2).toUpperCase() + value.slice(2);
    };
    CapitalizePipe.decorators = [
        { type: Pipe, args: [{
                    name: 'capitalize',
                },] },
    ];
    /** @nocollapse */
    CapitalizePipe.ctorParameters = function () { return []; };
    return CapitalizePipe;
}());
export { CapitalizePipe };
//# sourceMappingURL=capitalize.pipe.js.map