var FilterTable = (function () {
    function FilterTable() {
        this._filteredData = [];
        this.currentPage = 0;
        this._data = [];
    }
    Object.defineProperty(FilterTable.prototype, "filteredData", {
        get: function () {
            return this._filteredData;
        },
        set: function (data) {
            if (this._filteredData && data && this._filteredData.length !== data.length) {
                this.currentPage = 0;
            }
            this._filteredData = data;
        },
        enumerable: true,
        configurable: true
    });
    return FilterTable;
}());
export { FilterTable };
//# sourceMappingURL=filter-table.js.map