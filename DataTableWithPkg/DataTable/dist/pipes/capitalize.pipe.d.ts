import { PipeTransform } from "@angular/core";
export declare class CapitalizePipe implements PipeTransform {
    transform(value: string): string;
}
