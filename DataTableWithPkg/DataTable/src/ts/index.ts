import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTableComponent } from './components/data-table/data-table.component';
import { FilteredTableComponent } from './components/filtered-table/filtered-table.component';
import { PaginatedTableComponent } from './components/paginated-table/paginated-table.component';
import { DataFormatPipePipe } from './pipes/data-format-pipe.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [DataTableComponent, FilteredTableComponent, PaginatedTableComponent, DataFormatPipePipe],
  exports: [DataTableComponent, FilteredTableComponent, PaginatedTableComponent],
})
export class DataTableModule { }

import { FilterForm } from './models/filter-form';

export { FilterForm };
