import { Component, Input, forwardRef } from '@angular/core';

import { DataTableCol } from '../../models/data-table-col';
import { PageableTable } from '../../models/pageable-table';

@Component({
  selector: 'data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css'],
  providers: [
    { provide: PageableTable, useExisting: forwardRef(() => DataTableComponent) },
  ],
})
export class DataTableComponent extends PageableTable {

  @Input()
  public config: {
    cols: DataTableCol[],
  } = { cols: [] };

  @Input()
  public set data(d: any[]) {
    this._allData = d;
    this._pageOfData = d;
  }

  public get data() {
    return this._pageOfData;
  }
}
