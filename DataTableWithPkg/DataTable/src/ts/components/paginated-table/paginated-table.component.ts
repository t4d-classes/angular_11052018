import {
  Component, OnInit, forwardRef, Input, ContentChild,
  AfterContentInit, AfterContentChecked
} from '@angular/core';

import { FilterTable } from '../../models/filter-table';
import { PageableTable } from '../../models/pageable-table';


@Component({
  selector: 'paginated-table',
  templateUrl: './paginated-table.component.html',
  styleUrls: ['./paginated-table.component.css'],
  providers: [
    { provide: FilterTable, useExisting: forwardRef(() => PaginatedTableComponent) }
  ],
})
export class PaginatedTableComponent extends FilterTable implements OnInit, AfterContentInit, AfterContentChecked {

  @Input()
  public set data(d: any[]) {
    this._data = d || [];
    this.pageableTable.dataPage = this.listPage;
  }

  public get data() {
    return this._data;
  }

  @Input()
  public pageLength = 10;

  @Input()
  public initialPage = 0;

  @Input()
  public listPropName = 'data';

  @ContentChild(PageableTable)
  public pageableTable: PageableTable;

  public get allData() {
    return this.pageableTable.allData;
  }

  public get listPage(): any[] {
    const startIndex = this.currentPage * this.pageLength;
    const endIndex = startIndex + this.pageLength;
    return (this.filteredData || this.data).slice(startIndex, endIndex);
  }

  public get totalPages(): number {
    return Math.ceil((this.filteredData || this.data).length / this.pageLength);
  }

  public get firstPage() {
    return this.currentPage <= 0;
  }

  public get lastPage() {
    return this.currentPage + 1 >= this.totalPages;
  }

  public ngOnInit() {
    this.currentPage = this.initialPage;
  }

  public ngAfterContentInit() {
    this.data = this.pageableTable.allData;
  }

  public ngAfterContentChecked() {
    if (this.data !== this.pageableTable.allData) {
      this.data = this.pageableTable.allData;
      this.filteredData = null;
      this.currentPage = this.initialPage;
    }
    this.pageableTable.dataPage = this.listPage;
  }

  public go(pages: number) {
    this.currentPage += pages;
    this.pageableTable.dataPage = this.listPage;
  }
}
