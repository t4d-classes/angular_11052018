export abstract class PageableTable {

  protected _allData: any[];
  protected _pageOfData: any[];

  get allData() {
    return this._allData || [];
  }

  set dataPage(d: any[]) {
    this._pageOfData = d;
  }
}
