export abstract class FilterTable {

  private _filteredData: any[] = [];
  public currentPage = 0;

  protected _data: any[] = [];

  public get filteredData() {
    return this._filteredData;
  }

  public set filteredData(data: any[]) {
    if (this._filteredData && data && this._filteredData.length !== data.length) {
      this.currentPage = 0;
    }
    this._filteredData = data;
  }

  abstract get allData(): any[];
}
