export abstract class FilterForm {
  public filterValue: any;
  abstract clearForm(): void;
}
