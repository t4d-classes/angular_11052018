import { Injectable } from '@angular/core';

import { Car } from '../models/car';

@Injectable()
export class CarsService {

  private _cars: Car[] = [];

  constructor() { }

  append(car: Car) {
    this._cars.push(car);
    return this;
  }

  all() {
    return this._cars;
  }

}
