import { TestBed, ComponentFixture } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

import { ItemListComponent } from './item-list.component';

describe('item list component tests', () => {

  let fixture: ComponentFixture<ItemListComponent>;
  let component: ItemListComponent;
  let de: DebugElement;
  let el: HTMLElement;

  beforeEach(() => {

    TestBed.configureTestingModule({
      declarations: [ItemListComponent],
    });

    fixture = TestBed.createComponent(ItemListComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement.query(By.css('ul'));
    el = de.nativeElement;

  });

  it('item list populated', () => {

    component.items = ['item 1', 'item 2', 'item 3'];
    fixture.detectChanges();

    expect(component.items.length).toBe(3);
  });

  it('item list ui built correctly', () => {

    component.items = ['item 1', 'item 2', 'item 3'];
    fixture.detectChanges();

    expect(el.childElementCount).toBe(3);
  });

});
