Exercise 10

1. Create a new pipe named ellipsis. Ellipsis take are the first argument the text being piped in. The second argument will be length of text to show before showing an ellipsis. Only show the ellipsis if it exceeds the length specified.

2. Add a text area to the page where the text can be typed in. Beneath the text area show the text piped through the ellipsis pipe.

3. Add a number input box where you can change the length of the text used for the ellipsis.

4. Ensure it works.


