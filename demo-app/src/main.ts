import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));

// import { Observable, Observer, interval, timer } from 'rxjs';
// import { map, filter, take, skip, concat, merge } from 'rxjs/operators';

// const first = interval(500).pipe(take(10), map(n => 'first-' + String(n)));
// const second = interval(500).pipe(take(10), map(n => 'second-' + String(n)));

// first.pipe(merge(second)).subscribe(x => console.log(x));




// const first = timer(500).pipe(map(n => 'first-' + String(n)));


// interval(500).pipe(
//   map(n => n * 2),
//   filter(n => n > 20),
//   skip(3),
//   take(1),
// ).subscribe(num => console.log(num), null, () => {
//   console.log('completed');
// });


// let counter = 0;

// const nums$: Observable<number> = Observable.create((observer: Observer<number>) => {

//   const myCounter = counter++;

//   console.log('called observable.create', myCounter);

//   let num = 0;

//   const h = setInterval(() => {
//     num++;

//     console.log('emitting');

//     if (num > 3) {
//       observer.error('too many nums');
//       return;
//     }

//     observer.next(num);
//   }, 500);

//   setTimeout(() => {
//     clearInterval(h);
//     observer.complete();
//   }, 4000);

// });

// const numsSubscription = nums$.subscribe(results => {
//   console.log(results);
// }, null, () => {
//   console.log('nums complete');
// });



// nums$.subscribe(results => {
//   // console.log(results);
// });

// const p = new Promise(resolve => resolve('a'));

// p.then(() => {})
// p.then(() => {})


