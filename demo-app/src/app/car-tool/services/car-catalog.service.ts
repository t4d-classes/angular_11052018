import { Injectable } from '@angular/core';

import { MakeModelsArray } from '../models/make-models';

@Injectable({
  providedIn: 'root',
  // useValue: { makeModels: Object.freeze([
  //   { name: 'Ford', models: ['F-150', 'Fusion Hydrid', 'Focus'] },
  //   { name: 'Subaru', models: ['Juste'] },
  //   { name: 'Lancia', models: ['Stratos'] },
  //   { name: 'Jeep', models: ['Cherokee', 'Wrangler'] },
  //   { name: 'Lamborghini', models: ['Diablo', 'Aventedor'] }
  // ]) as MakeModelsArray },
})
export class CarCatalogService {

  makeModels: MakeModelsArray;
}
