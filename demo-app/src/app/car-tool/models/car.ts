export interface Car {
  id?: number;
  make: string;
  model: string;
  year: number;
  color: string;
  price: number;
}

export type ReadonlyCars = ReadonlyArray<Car>;

export type MutableCars = Array<Car>;

export type Cars = ReadonlyCars | MutableCars;
