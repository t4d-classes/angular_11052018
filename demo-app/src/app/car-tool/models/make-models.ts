export interface MakeModels {
  name: string;
  models: string[];
}

export type MakeModelsArray = ReadonlyArray<MakeModels> | Array<MakeModels>;

