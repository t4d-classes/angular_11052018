import { Component, OnInit } from '@angular/core';

import { Car, ReadonlyCars } from '../../models/car';
import { CarsService } from '../../services/cars.service';

@Component({
  selector: 'car-home',
  templateUrl: './car-home.component.html',
  styleUrls: ['./car-home.component.css']
})
export class CarHomeComponent implements OnInit {

  cars: ReadonlyCars = [];

  editCarId = -1;

  constructor(private carsSvc: CarsService) { }

  ngOnInit() {
    this.cars = this.carsSvc.all();
  }

  doAdd(car: Car) {
    this.cars = this.carsSvc.append(car).all();
    this.editCarId = -1;
  }

  doDelete(carId: number) {
    this.cars = this.carsSvc.delete(carId).all();
    this.editCarId = -1;
  }

  doEdit(carId: number) {
    this.editCarId = carId;
  }

  doCancel() {
    this.editCarId = -1;
  }

  doSave(car: Car) {
    this.cars = this.carsSvc.replace(car).all();
    this.editCarId = -1;
  }

}
