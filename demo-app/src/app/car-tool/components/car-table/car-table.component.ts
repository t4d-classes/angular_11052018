import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { memoize, uniqueId } from 'lodash';

import { Car, Cars, MutableCars } from '../../models/car';

const carsUniqueId = Symbol('unique id');

@Component({
  selector: 'car-table',
  templateUrl: './car-table.component.html',
  styleUrls: ['./car-table.component.css'],
})
export class CarTableComponent implements OnInit {

  _lastCars: Cars = [];
  _cars: MutableCars = [];

  @Input()
  editCarId = -1;

  sortField = 'id';

  @Output() editCar = new EventEmitter<number>();
  @Output() deleteCar = new EventEmitter<number>();
  @Output() saveCar = new EventEmitter<Car>();
  @Output() cancelCar = new EventEmitter<void>();

  // defining the field
  sortCars: (cars: Cars, fieldName: string) => Cars;

  constructor() {

    // assignment
    this.sortCars = memoize((cars, fieldName) => {
      // either cars changed or the field has not been sorted yet
      console.log('sortCars: ', fieldName);
      return cars.concat().sort((a, b) => {
        if (a[fieldName] < b[fieldName]) {
          return -1;
        } else if (a[fieldName] > b[fieldName]) {
          return 1;
        } else {
          return 0;
        }
      });
    }, (cars, fieldName) => cars[carsUniqueId] + fieldName);

  }

  ngOnInit() {
  }

  doSort(fieldName: string) {
    // console.log('doSort', fieldName);
    this.sortField = fieldName;
  }

  @Input()
  set cars(cars: Cars) {
    if (cars !== this._lastCars) {
      this._lastCars = cars;
      this._cars = cars.concat();
      this._cars[carsUniqueId] = uniqueId('cars-');
    }
  }

  get sortedCars() {
    // console.log('sortedCars: ', this.sortField);
    return this.sortCars(this._cars, this.sortField);
  }

  // doDelete(carId: number) {
  //   this.deleteCar.emit(carId);
  // }

}
