export interface Color {
  id?: number;
  name: string;
  hexCode: string;
}

export type ReadonlyColors = ReadonlyArray<Color>;

export type MutableColors = Array<Color>;

export type Colors = ReadonlyColors | MutableColors;
