import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColorInfoControlComponent } from './color-info-control.component';

describe('ColorInfoControlComponent', () => {
  let component: ColorInfoControlComponent;
  let fixture: ComponentFixture<ColorInfoControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColorInfoControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColorInfoControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
