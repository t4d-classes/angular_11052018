import { Component, OnInit } from '@angular/core';
import { FormControl, NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

@Component({
  selector: 'color-info-control',
  templateUrl: './color-info-control.component.html',
  styleUrls: ['./color-info-control.component.css'],
  providers: [
    // CarsService - shortcut for the below
    // { provide: CarsService, useClass: CarsService },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting:  ColorInfoControlComponent,
      multi: true,
    }
  ],
})
export class ColorInfoControlComponent implements OnInit, ControlValueAccessor {

  shadeInput = new FormControl();
  colorInput = new FormControl();

  private _onChange: (colorInfo: { shade: string, color: string }) => void;
  private _onTouched: () => void;

  constructor() { }

  ngOnInit() {
  }

  doInput() {
    this._onChange({
      shade: this.shadeInput.value,
      color: this.colorInput.value,
    });
  }

  doBlur() {
    this._onTouched();
  }

  writeValue(colorInfo: { shade: string, color: string }) {
    console.log('am I called?');
    this.shadeInput.setValue(colorInfo.shade);
    this.colorInput.setValue(colorInfo.color);
  }

  registerOnChange(fn: any) {
    this._onChange = fn;
  }

  registerOnTouched(fn: any) {
    this._onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean) {

    if (isDisabled) {
      this.shadeInput.disable();
      this.colorInput.disable();
    } else {
      this.shadeInput.enable();
      this.colorInput.enable();
    }

  }
}
