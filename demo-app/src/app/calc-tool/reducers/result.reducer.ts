
import { CalcActions, CalcActionsUnion } from '../actions/calc.actions';

export const reducer = (state = 0, action: CalcActionsUnion) => {

  switch (action.type) {
    case CalcActions.Reset:
      return 0;
    case CalcActions.Add:
      return state + action.payload;
    case CalcActions.Subtract:
      return state - action.payload;
    case CalcActions.Multiply:
      return state * action.payload;
    case CalcActions.Divide:
      return state / action.payload;
    default:
      return state;
  }

};
