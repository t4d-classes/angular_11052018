import { Pipe, PipeTransform } from '@angular/core';
import { isNil } from 'lodash';

@Pipe({
  name: 'ellipsis'
})
export class EllipsisPipe implements PipeTransform {

  transform(text: any, lengthBeforeEllipsis: any): any {

    if (isNil(text)) {
      return '';
    }

    if (isNil(lengthBeforeEllipsis)) {
      return text;
    }

    const sText = String(text);
    const nLengthBeforeEllipsis = Number(lengthBeforeEllipsis);

    if (isNaN(nLengthBeforeEllipsis)) {
      return text;
    }

    if (sText.length > nLengthBeforeEllipsis) {
      return sText.slice(0, nLengthBeforeEllipsis) + '...';
    }

    return sText;
  }

}
